//
//  NewsArticleTests.swift
//  NewsArticleTests
//
//  Created by Nirav on 08/05/21.
//

import XCTest
@testable import NewsArticle

class NewsArticleTests: XCTestCase {

    var newsListCoordinator : NewsListCoordinator!
    var newsListViewController : NewsListViewController!
    
    override func setUp() {
        super.setUp()
        
        let navigationController = UINavigationController()
        newsListCoordinator = NewsListCoordinator(navigationController: navigationController)
        newsListCoordinator.start()
        
        newsListViewController = Storyboard.Main().instatiate(.newsListViewController) as? NewsListViewController
        let viewModel = NewsListViewModel()
        viewModel.coordinator = newsListCoordinator
        newsListViewController.viewModel = viewModel
        newsListViewController.loadViewIfNeeded()
    }

    override func tearDown() {
        newsListCoordinator = nil
        newsListViewController = nil
        super.tearDown()
    }

    func test_VCHasTableView() {
        XCTAssertNotNil(newsListViewController.tblNewsList)
    }

    func test_TableViewHasDelegate() {
        XCTAssertNotNil(newsListViewController.tblNewsList.delegate)
    }
    
    func test_TableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(newsListViewController.conforms(to: UITableViewDelegate.self))
        //XCTAssertTrue(viewControllerUnderTest.responds(to: #selector(viewControllerUnderTest.tableView(_:didSelectRowAt:))))
    }
    
    func test_TableViewHasDataSource() {
        XCTAssertNotNil(newsListViewController.tblNewsList.dataSource)
    }
    
    func test_TableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(newsListViewController.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(newsListViewController.responds(to: #selector(newsListViewController.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(newsListViewController.responds(to: #selector(newsListViewController.tableView(_:cellForRowAt:))))
    }
    
    func test_API_GetNewsList() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        
        APIService.shared.API_getNewsList {(resp,success,error) in
            XCTAssertNil(error)
            XCTAssertNotNil(resp)
            ex.fulfill()
            
        }
        
        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}
