//
//  APIService.swift
//  MVVM_New
//
//  Created by Abhilash Mathur on 20/05/20.
//  Copyright © 2020 Abhilash Mathur. All rights reserved.
//

import Foundation
import Alamofire

class APIService {
    
    static let shared = APIService()
    
    private let sourcesURL = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=61b38de9fc054279bd0cf6c6fe6a2822")!
    
    func API_getNewsList(completionHandler: @escaping (_ resp: Any?,
                                                       _ success: Bool,
                                                       _ error: Any?)->Swift.Void)
    {
        if !isConnectedToInternet()
        {
            completionHandler(nil,false,nil)
            return
        }
        
        let headers = NSMutableDictionary()
        headers.setValue("application/json", forKey: "Content-Type")
        
        let hed : HTTPHeaders = HTTPHeaders.init(headers as! [String : String])
        
        AF.request(sourcesURL,
                   method: .get ,
                   parameters:nil,
                   encoding:JSONEncoding.default,
                   headers: hed).responseJSON { res in

            switch res.result {
            case .success(let value):
                let tempDict = value as! NSDictionary
                print(tempDict)
                if tempDict.value(forKey: "status") as! String == "error" {
                    let errorMessage = tempDict.value(forKey: "message")
                    completionHandler(nil, false, errorMessage)
                } else {
                    completionHandler(value, true, nil)
                }
                break
            case .failure(let error):
                //print(error)
                completionHandler(nil, false, error)
                break
                //showAPIMessageAlert(title: "", message: res.error!.localizedDescription)
            
            }
        }
    }
    
    // MARK: - Rechability Functions
    func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}
