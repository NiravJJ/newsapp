//
//	ModelNewsList.swift
//	

import Foundation


class ModelNewsList : NSObject, NSCoding{

	var articles : [ModelArticle]!
	var status : String!
	var totalResults : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		articles = [ModelArticle]()
		if let articlesArray = dictionary["articles"] as? [NSDictionary]{
			for dic in articlesArray{
				let value = ModelArticle(fromDictionary: dic)
				articles.append(value)
			}
		}
		status = dictionary["status"] as? String == nil ? "" : dictionary["status"] as? String
		totalResults = dictionary["totalResults"] as? Int == nil ? 0 : dictionary["totalResults"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if articles != nil{
			var dictionaryElements = [NSDictionary]()
			for articlesElement in articles {
				dictionaryElements.append(articlesElement.toDictionary())
			}
			dictionary["articles"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		if totalResults != nil{
			dictionary["totalResults"] = totalResults
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         articles = aDecoder.decodeObject(forKey: "articles") as? [ModelArticle]
         status = aDecoder.decodeObject(forKey: "status") as? String
         totalResults = aDecoder.decodeObject(forKey: "totalResults") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if articles != nil{
			aCoder.encode(articles, forKey: "articles")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if totalResults != nil{
			aCoder.encode(totalResults, forKey: "totalResults")
		}

	}

}
