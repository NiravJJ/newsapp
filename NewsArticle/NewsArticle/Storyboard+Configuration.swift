//
//  Storyboard+Configuration.swift
//  NewsArticle
//
//  Created by Nirav on 08/05/21.
//

import UIKit

enum Storyboard {
    
    static private func instantiateViewController(_ type: String, from storyBoardName: String) -> UIViewController {
        let id = type
        let storyboard = initialiseStoryboard(from: storyBoardName)
        let viewController = storyboard.instantiateViewController(withIdentifier: id)
        return viewController
    }
    
    static private func initialiseStoryboard(from name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }
}

// Main
extension Storyboard {
    
    struct Main {
        private let name = "Main"
        
        enum ViewControllerType: String {
            case newsListViewController
            case newsDetailViewController
        }
        
        func instatiate(_ type: ViewControllerType) -> UIViewController {
            return instantiateViewController(type.rawValue, from: name)
        }
    }
}
