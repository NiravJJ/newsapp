//
//  NewsDetailViewController.swift
//  NewsArticle
//
//  Created by Nirav on 11/05/21.
//

import UIKit
import SDWebImage

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var imgNewsPic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    var viewModel: NewsDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLeftBarButtonWithImage(image: UIImage(named: "back")!, withSelector: "onNavigateBack")
        callViewModelForUIUpdate()
    }
 
    func callViewModelForUIUpdate() {
        self.title = viewModel.title
        
        imgNewsPic.sd_setImage(with: URL(string: viewModel.objArticle.urlToImage), placeholderImage: UIImage(named: "placeholderImg"), options: SDWebImageOptions.queryMemoryDataSync)
        lblTitle.text = viewModel.objArticle.title
        lblAuthor.text = "Source: " + viewModel.objArticle.source.name
        lblDesc.text = viewModel.objArticle.descriptionField
        lblContent.text = viewModel.objArticle.content
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        imgNewsPic.isUserInteractionEnabled = true
        imgNewsPic.addGestureRecognizer(tap)
    }
    
    @IBAction func onReadMorePress(_ sender: UIButton) {
        
        if let url = URL(string: viewModel.objArticle.url) {
            UIApplication.shared.open(url)
        }
    }
    
}
