//
//  NewsDetailCoordinator.swift
//  NewsArticle
//
//  Created by Nirav on 11/05/21.
//

import UIKit

final class NewsDetailCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var navigationController : UINavigationController
    var parentCoordinator: NewsListCoordinator?
    
    var objArticle : ModelArticle!
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let newsDetailViewController = Storyboard.Main().instatiate(.newsDetailViewController) as! NewsDetailViewController
        let newsDetailViewModel = NewsDetailViewModel()
        newsDetailViewModel.coordinator = self
        newsDetailViewModel.objArticle = objArticle
        newsDetailViewController.viewModel = newsDetailViewModel
        navigationController.pushViewController(newsDetailViewController, animated: true)
    }
    
    func didFinish() {
        parentCoordinator?.childDidFinish(self)
    }
}
