//
//  NewsListCoordinator.swift
//  NewsArticle
//
//  Created by Nirav on 08/05/21.
//

import UIKit

final class NewsListCoordinator: Coordinator {
    
    private(set) var childCoordinators: [Coordinator] = []
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.navigationController.navigationBar.barTintColor = .black
        self.navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    func start() {
        let newsListViewController = Storyboard.Main().instatiate(.newsListViewController) as! NewsListViewController
        let viewModel = NewsListViewModel()
        viewModel.coordinator = self
        newsListViewController.viewModel = viewModel
        navigationController.setViewControllers([newsListViewController], animated: false)
    }
    
    func showNewsDetailCoordinator(objArticle : ModelArticle) {
        let newsDetailCoordinator = NewsDetailCoordinator(navigationController: navigationController)
        newsDetailCoordinator.parentCoordinator = self
        newsDetailCoordinator.objArticle = objArticle
        childCoordinators.append(newsDetailCoordinator)
        newsDetailCoordinator.start()
    }
    
    func childDidFinish(_ childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: { (coordinator) -> Bool in
            return coordinator === childCoordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
    
}

