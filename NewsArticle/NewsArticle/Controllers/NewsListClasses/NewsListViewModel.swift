//
//  NewsListViewModel.swift
//  NewsArticle
//
//  Created by Nirav on 08/05/21.
//


import UIKit

final class NewsListViewModel : NSObject {
    
    var updateList: ((_ success : Bool, _ Error : Any?) -> Void) = {_,_ in }
    
    var coordinator: NewsListCoordinator?
    
    private(set) var newsList = [Cell]()
    enum Cell {
        case NewsListTVCell(ModelArticle)
    }
    var title = "Headlines"
    var tblIdentifier = "tblNewsList"
    var objNewsArticle : ModelNewsList!
    
    override init() {
        super.init()
    }
    
    func viewDidLoad() {
    }
    
    //MARK: collection methods
    
    func reloadUI() {
        loadNewsList()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            self.callWebService()
        })
    }
    
    func callWebService() {
        self.newsList = []
        var newCells = [Cell]()
        
        if !APIService.shared.isConnectedToInternet() {
            if let objSavedData = UserDefaultsMethods.getCustomObject(key: kSaveNewsList)  {
                objNewsArticle = ModelNewsList.init(fromDictionary: objSavedData as! NSDictionary)
                for obj in objNewsArticle.articles {
                    newCells.append(Cell.NewsListTVCell(obj))
                }
                
                self.newsList.append(contentsOf: newCells)
                self.updateList(true,nil)
            }
            else {
                self.updateList(false,"Please connect to Internet!")
            }
        } else {
            APIService.shared.API_getNewsList {(resp,success,error) in
                if success == true {
                    
                    self.objNewsArticle = ModelNewsList.init(fromDictionary: resp as! NSDictionary)
                    UserDefaultsMethods.setCustomObject(value:resp as! NSDictionary, key: kSaveNewsList)
                    
                    for obj in self.objNewsArticle.articles {
                        newCells.append(Cell.NewsListTVCell(obj))
                    }
                    
                    self.newsList.append(contentsOf: newCells)
                }
                self.updateList(success,error)
            }
        }
        
    }
    
    func loadNewsList() {
        newsList = []
        var newCells = [Cell]()
        for _ in 0...4 {
            newCells.append(Cell.NewsListTVCell(ModelArticle()))
        }
        newsList.append(contentsOf: newCells)
    }
    
    func numberOfRows(section: Int) -> Int {
        return newsList.count
    }
    
    func cell(for indexPath: IndexPath) -> Cell {
        return newsList[indexPath.row]
    }
    
    func didSelect(indexPath: IndexPath) {
        coordinator?.showNewsDetailCoordinator(objArticle: objNewsArticle.articles[indexPath.row])
        /*switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                coordinator?.showDutyStatusCoordinator()
                break
            case 2:
                coordinator?.showReportsCoordinator()
                break
            case 3:
                coordinator?.showDispatchCoordinator()
                break
            default:
                break
            }
        default:
            break
        }*/
    }
}

