//
//  NewsListViewController.swift
//  NewsArticle
//
//  Created by Nirav on 08/05/21.
//

import UIKit
import ListPlaceholder

class NewsListViewController: UIViewController {

    @IBOutlet weak var tblNewsList: UITableView!
    @IBOutlet weak var imgNoDataFound: UIImageView!
    
    var viewModel: NewsListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callViewModelForUIUpdate()
        tableLoaderAnimation(show: true)
        handleUpdates()
    }
    
    func callViewModelForUIUpdate() {
        self.title = viewModel.title
        tblNewsList.accessibilityIdentifier = viewModel.tblIdentifier
        self.viewModel.reloadUI()
    }
    
    func tableLoaderAnimation(show:Bool) {
        if show {
            tblNewsList.reloadData()
            tblNewsList.showLoader()
        } else {
            tblNewsList.hideLoader()
        }
    }
    
    private func handleUpdates() {
        viewModel.updateList = {(success, error) in
            self.tableLoaderAnimation(show: false)
            self.tblNewsList.reloadData()
            
            if !success {
                self.imgNoDataFound.isHidden = false
                let alert = AlertController()
                alert.showAlert(message: error as! String, sender: self, okCompletion: nil)
            }
        }
    }
}

extension NewsListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(section: 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch viewModel.cell(for: indexPath) {
        case .NewsListTVCell(let viewModel):
            let listCell = tableView.dequeueReusableCell(withIdentifier: NewsListTVCell.identifier, for: indexPath) as! NewsListTVCell
            listCell.configure(viewModel: viewModel)
            return listCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
}
