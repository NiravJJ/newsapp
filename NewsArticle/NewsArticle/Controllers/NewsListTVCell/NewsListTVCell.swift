//
//  NewsListTVCell.swift
//  NewsArticle
//
//  Created by Nirav on 08/05/21.
//

import UIKit
import SDWebImage

class NewsListTVCell: UITableViewCell {
    
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var identifier : String {
        return String(describing: self)
    }
    
    func configure(viewModel: ModelArticle) {
        
        if viewModel.title != nil && viewModel.title.count > 0 {
            lblTitle.text = viewModel.title
            lblSubTitle.text = viewModel.descriptionField
        } else {
            lblTitle.text = "Title"
            lblSubTitle.text = "description"
        }
        
        let strImageURL = viewModel.urlToImage
        if let image = SDImageCache.shared.imageFromMemoryCache(forKey: strImageURL) {
            imgPic.image = image
        } else {
            if viewModel.urlToImage != nil {
                imgPic.sd_setImage(with: URL(string: strImageURL!), placeholderImage: UIImage(named: "placeholderImg"), options: SDWebImageOptions.queryMemoryDataSync) { (img, err, cacheType, url) in
                    SDImageCache.shared.store(img, forKey: strImageURL)
                }
            }
        }
    }

}
