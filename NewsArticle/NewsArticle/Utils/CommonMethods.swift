//
//  CommonMethods.swift
//  NiravExpiaDemo
//
//  Created by Nirav on 04/05/21.
//

import UIKit 

let kSaveNewsList : String = "kSaveNewsList"

class AlertController: UIAlertController {

    let keyWindows = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
    
    func showAlert(forTitle title: String = "", message: String, sender: UIViewController, okTitle: String = "OK", okCompletion: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .default) { _ in
            if okCompletion != nil {
                okCompletion!()
            }
        }
        alertController.addAction(okAction)
        keyWindows?.rootViewController?.present(alertController, animated: true, completion: nil)
        //sender.present(alertController, animated: true, completion: nil)
    }
}



class UserDefaultsMethods: NSObject {
    
 
// MARK: - Custom object Functions
    class func setCustomObject(value:AnyObject,key:String)
    {
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: true)
            UserDefaults.standard.set(data, forKey: key)
            UserDefaults.standard.synchronize()
        } catch (let error){
            #if DEBUG
            print("Failed to convert : \(error.localizedDescription)")
            #endif
        }
    }
    
    class func getCustomObject(key:String) -> Any?
    {
        let data = UserDefaults.standard.object(forKey: key) as? NSData
        if data == nil
        {
            return nil
        }
        
        do {
            if let value = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data! as Data) {
                return value
            }
        } catch {
            print("Couldn't read file.")
        }
        return nil
    }
    
    
    class func removeObjectForKey(_ objectKey: String) {
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: objectKey)
        defaults.synchronize()
    }
    
    class func removeAllKeyFromDefault(){
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
}

extension UIViewController
{
    func setLeftBarButtonWithImage(image : UIImage, withSelector selector : String)
    {
        self.navigationItem.hidesBackButton = true
        let leftButton : UIBarButtonItem = UIBarButtonItem(image: image.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: NSSelectorFromString(selector))
        leftButton.imageInsets = UIEdgeInsets.init(top: 0, left: -5 , bottom: 0, right: 0)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func onNavigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        
        self.navigationController?.isNavigationBarHidden = true
        
         UIView.transition(with: self.view,
                           duration: 0.25,
                           options: [.transitionCrossDissolve],
                           animations: {
                            self.view.addSubview(newImageView)                    
                           }, completion: nil)
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        
        
        UIView.transition(with: self.view,
                          duration: 0.25,
                          options: [.transitionCrossDissolve],
                          animations: {
                            sender.view?.removeFromSuperview()
                          }, completion: nil)
    }
    
    
}
